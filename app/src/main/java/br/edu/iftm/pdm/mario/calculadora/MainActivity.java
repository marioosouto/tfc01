package br.edu.iftm.pdm.mario.calculadora;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView textDisplayInput, textDisplayHistorico;
    private Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btnPonto;
    private Button somar, subtrair, multiplicar, dividir, igual;
    private String TAG = "MainActivity";
    boolean valida = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();
    }


    public void iniciarComponentes() {
        this.textDisplayInput = findViewById(R.id.textDisplayInput);
        this.textDisplayHistorico = findViewById(R.id.textDisplayHistorico);
        this.btn1 = findViewById(R.id.btn1);
        this.btn2 = findViewById(R.id.btn2);
        this.btn3 = findViewById(R.id.btn3);
        this.btn4 = findViewById(R.id.btn4);
        this.btn5 = findViewById(R.id.btn5);
        this.btn6 = findViewById(R.id.btn6);
        this.btn7 = findViewById(R.id.btn7);
        this.btn8 = findViewById(R.id.btn8);
        this.btn9 = findViewById(R.id.btn9);
        this.btnPonto = findViewById(R.id.btnPonto);
        this.somar = findViewById(R.id.btnSoma);
        this.subtrair = findViewById(R.id.btnSubtracao);
        this.multiplicar = findViewById(R.id.btnMultiplicacao);
        this.dividir = findViewById(R.id.btnDivisao);
        this.igual = findViewById(R.id.btnIgual);
    }

    public void acrescentarExpressao(String string) {
        if (textDisplayInput.getText().equals("")) {
            textDisplayHistorico.setText(" ");
        }
        if (valida) {
            textDisplayInput.setText(" ");
            textDisplayInput.append(string);
            valida = false;
        } else
            textDisplayInput.append(string);

    }

    public void estadoInicial(View view) {
        this.textDisplayHistorico.setText(" ");
        this.textDisplayInput.setText("0");
    }

    public static char ultimaLetra(String string) {
        if (string != null && string.length() > 0) {
            return string.charAt(string.length() - 1);
        }
        return (char) 0;
    }


    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btn0:
                    acrescentarExpressao("0");
                    break;

                case R.id.btn1:
                    acrescentarExpressao("1");
                    break;
                case R.id.btn2:
                    acrescentarExpressao("2");
                    break;
                case R.id.btn3:
                    acrescentarExpressao("3");
                    break;
                case R.id.btn4:
                    acrescentarExpressao("4");
                    break;
                case R.id.btn5:
                    acrescentarExpressao("5");
                    break;

                case R.id.btn6:
                    acrescentarExpressao("6");
                    break;
                case R.id.btn7:
                    acrescentarExpressao("7");
                    break;
                case R.id.btn8:
                    acrescentarExpressao("8");
                    break;
                case R.id.btn9:
                    acrescentarExpressao("9");
                    break;

                case R.id.btnPonto:
                    try {
                        char string = ultimaLetra(textDisplayInput.getText().toString());
                        if (string != '.')
                            if (textDisplayInput.getText().toString().equals("0")) {
                                textDisplayInput.setText("");
                                acrescentarExpressao("0.");
                            } else
                                acrescentarExpressao(".");
                        else
                            break;
                    } catch (Exception ex) {
                        Log.e(TAG, "Erro na função verificar: " + ex.getMessage());
                    }
                    break;

                case R.id.btnSoma:
                    try {
                        textDisplayHistorico.append(textDisplayInput.getText().toString().trim());
                        textDisplayInput.setText(" ");
                        valida = true;
                        Expression expressao = new ExpressionBuilder(textDisplayHistorico.getText().toString()).build();
                        Double res = expressao.evaluate(); //metodo avalia e trata todas as expressoes
                        textDisplayInput.setText(String.valueOf(res));

                        char string = ultimaLetra(textDisplayHistorico.getText().toString());
                        if (string != '+')
                            if (textDisplayInput.getText().equals("0"))
                                textDisplayHistorico.setText("0+");
                            else
                                textDisplayHistorico.append("+");
                        else
                            break;
                        break;
                    } catch (Exception ex) {
                        Log.e(TAG, "Erro na função verificar: " + ex.getMessage());
                    }
                    break;

                case R.id.btnSubtracao:
                    try {
                        textDisplayHistorico.append(textDisplayInput.getText().toString().trim());
                        textDisplayInput.setText(" ");
                        valida = true;
                        Expression expressao = new ExpressionBuilder(textDisplayHistorico.getText().toString()).build();
                        Double res = expressao.evaluate(); //metodo avalia e trata todas as expressoes
                        textDisplayInput.setText(String.valueOf(res));

                        char string = ultimaLetra(textDisplayHistorico.getText().toString());
                        if (string != '+')
                            if (textDisplayInput.getText().equals("0"))
                                textDisplayHistorico.setText("0-");
                            else
                                textDisplayHistorico.append("-");
                        else
                            break;
                        break;
                    } catch (Exception ex) {
                        Log.e(TAG, "Erro na função verificar: " + ex.getMessage());
                    }
                    break;
                case R.id.btnMultiplicacao:
                    try {
                        textDisplayHistorico.append(textDisplayInput.getText().toString().trim());
                        textDisplayInput.setText(" ");
                        valida = true;
                        Expression expressao = new ExpressionBuilder(textDisplayHistorico.getText().toString().trim()).build();
                        Double res = expressao.evaluate(); //metodo avalia e trata todas as expressoes
                        textDisplayInput.setText(String.valueOf(res));

                        char string = ultimaLetra(textDisplayHistorico.getText().toString());
                        if (string != '*')
                            if (textDisplayInput.getText().equals("0"))
                                textDisplayHistorico.setText("0*");
                            else
                                textDisplayHistorico.append("*");
                        else
                            break;
                        break;
                    } catch (Exception ex) {
                        Log.e(TAG, "Erro na função verificar: " + ex.getMessage());
                    }

                    break;

                case R.id.btnDivisao:
                    try {
                        textDisplayHistorico.append(textDisplayInput.getText().toString().trim());
                        textDisplayInput.setText(" ");
                        valida = true;
                        Expression expressao = new ExpressionBuilder(textDisplayHistorico.getText().toString()).build();
                        Double res = expressao.evaluate(); //metodo avalia e trata todas as expressoes
                        textDisplayInput.setText(String.valueOf(res));

                        char string = ultimaLetra(textDisplayHistorico.getText().toString());
                        if (string != '/')
                            if (textDisplayInput.getText().equals("0"))
                                textDisplayHistorico.setText("0/");
                            else
                                textDisplayHistorico.append("/");
                        else
                            break;
                        break;
                    } catch (Exception ex) {
                        Log.e(TAG, "Erro na função verificar: " + ex.getMessage());
                    }
                    break;
                case R.id.btnIgual:
                    try {
                        textDisplayHistorico.append(textDisplayInput.getText().toString().trim());
                        valida = true;
                        Expression expressao = new ExpressionBuilder(textDisplayHistorico.getText().toString()).build();
                        Double res = expressao.evaluate(); //metodo avalia e trata todas as expressoes
                        textDisplayInput.setText(String.valueOf(res));
                        textDisplayHistorico.setText(" ");
                    } catch (Exception ex) {
                        Log.e(TAG, "Erro na função verificar: " + ex.getMessage());
                    }
                    break;
            }
        } catch (Exception ex) {
            Log.e(TAG, "Erro na função verificar: " + ex.getMessage());
        }
    }
}
